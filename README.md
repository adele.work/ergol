#  Ergol

## Simple php gemini server

Ergol is multi-capsules Gemini server. It manages a TLS certificate for each capsule. It's very easy to create a shared service to host and serve Gemini capsules.

[Pollux.casa](https://pollux.casa/) uses ergol and [ergol-http (for http/https rendering)](https://codeberg.org/adele.work/ergol-http)

## Installation

* Create a dedicated folder (eg ergol)
* Copy ergol.php in this folder
* Copy ergol-sample.json to ergol.json in the same folder
* create a subdirectoy "capsules"
* and a sub-subdirectory in it for each capsule you want to host
* generate a certificate for each capsules (or use the same)

Exemple of installation directories/files tree :

``` 
ergol/
	ergol.php
	ergol.json
	capsules/
		localhost.pem
		localhost/
			index.gmi
			text.md
			subdir/
			...
		example.tld.pem
		example.tld
			index.gmi
			image.jpg
			favicon.txt
			tinylog.gmi
			gemolog/
				index.gmi
				...
```

## Configuration ergol.json

This file must be in the same folder than ergol.php

* interface : ip interface to listen to (0.0.0.0 for all, 127.0.0.1 for only local connexion)
* port : tcp port, 1965 usally for gemini protocol
* capsules : an array of all hosted capsules
	* cert : path to pem certificate (see below to generate one)
	* folder : root folder of the capsule ({here} is automatically replaced by fodler where ergol.php is)
	* lang : default lang code append to meta text file (mime : text/*)
	* lang_regex : regular expression to detect lang code in file name to replace default lang (ex: "/\\.([a-z][a-z])\\./" will find "fr" in "index.fr.gmi")
	* auto_index_ext : generate auto index for folder not including an index.gmi file. false means no autoindex, else an array of file ext included in the index ("/" means include subdir)
	* redirect : if you want to redirect client to another capsule, put scheme and hostname (ex : "gemini://another.example.tld"), requested path will be automatically appended.

autoindex add modification date of .gmi files if name does not start by YYYY-MM-DD format.
[See subscriptions specs](gemini://gemini.circumlunar.space/docs/companion/subscription.gmi)

## Generate a self-signed cert with openssl

Replace example.tld by your capsule host name. And eventually adapt certificate fields :

* /CN=	Must be your fully qualified host name
* /C=	Your country (ISO-3166 alpha/2)
* /ST=	Your state
* /L=	Your location
* /O=	Your organization
* /OU=	Organizational Unit

```
openssl req -x509 -newkey rsa:4096 \
 -subj '/CN=example.tld/C=FR/ST=My state/L=My city/O=My org/OU=Org Unit' \
 -nodes -keyout capsules/example.tld.pem -out capsules/example.tld.pem -days 730
```

## Start ergol server

In a terminal :

```
php ergol.php
```
or
```
php8 ergol.php
```
or
```
php7-cli ergol.php
```
or
```
chmod +x ergol.php
./ergol.php
``` 

Certainly easy to start it at boot or in systemd...
