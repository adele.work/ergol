#!/usr/bin/php
<?php
/**
 * #  ergol
 * 
 * Simple php gemini server.
 * 
 * - only one source file ergol.php
 * - manage several capsules with different cerificates
 * - generate index if no index.gmi file
 * - configuration in ergol.json file (see ergol.gmi for details)
 * 
 * gemini://adele.work/code/ergol/
 * 
 * Version 0.4.2
 * 
 * Copyright 2021 Adële
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a 
 * copy of this software and associated documentation files (the 
 * "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, 
 * distribute, sublicense, and/or sell copies of the Software, and to 
 * permit persons to whom the Software is furnished to do so, subject to 
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included 
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY 
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 */

main();
exit();

function main() 
{
	echo "Loading config...\n";

	$conf_json = file_get_contents(__DIR__."/ergol.json");
	if($conf_json===false)
		die("Unable to open file ".__DIR__."/ergol.json\n");

	$conf_json = preg_replace('/[\x00-\x1F\x80-\xFF]/', '',$conf_json);

	$conf = json_decode($conf_json);
	if($conf===null)
		die("Unable to parse ".__DIR__."/ergol.json : ".json_last_error_msg()."\n");

	if(function_exists('pcntl_fork'))
		echo "Multithreading enabled.\n";
	else
		echo "Multithreading disabled (PCNTL extension not present).\n";

	echo "Starting ergol server...\n";

	// create SNI certificates context 
	$certs = array();
	foreach($conf->capsules as $hostname => $capsule)
	{
		echo "-- ".$hostname."\n";
		
		if(empty($conf->capsules->$hostname->redirect))
		{
			$conf->capsules->$hostname->folder = str_replace("{here}",__DIR__,$capsule->folder);
			@mkdir($conf->capsules->$hostname->folder);
		}
		else
		{
			unset($conf->capsules->$hostname->folder);
		}
		
		$certs[$hostname] = str_replace("{here}",__DIR__,$capsule->cert);
		if(!file_exists($certs[$hostname]))
		{
			echo "Cert ".$certs[$hostname]." is missing !\n";
			echo "You could create it with :\n\n";
			echo "mkdir -p ".dirname($certs[$hostname])."\n\n";
			echo "openssl req -x509 -newkey rsa:4096 -subj '/CN=".$hostname;
			echo "/C=FR/ST=My state/L=My city/O=My org/OU=Organizational Unit'";
			echo " -nodes -keyout ".$certs[$hostname]." -out ".$certs[$hostname]." -days 730\n\n";
			echo "then restart ergol.php\n";
			unset($conf->capsules->$hostname);
			continue;
		}
	}
	$context = stream_context_create(
		array(
			'ssl' => array(
				'verify_peer' => false,
				'SNI_enabled' => true,
				'SNI_server_certs' => $certs
			)
		)
	);

	echo "Connect socket server...";

	try {
		$socket = stream_socket_server(
			'tcp://'.$conf->interface.':'.$conf->port,
			$errno,
			$errstr,
			STREAM_SERVER_BIND | STREAM_SERVER_LISTEN,
			$context,
		);
		if ($socket === false)
		{
			throw new \Exception($errstr, $errno);
		}
	}
	catch(Exception $ex)
	{
		die($e . "\n");
	}

	echo "Listening on ".$conf->interface.':'.$conf->port."\n";

	while (true)
	{
		
		$conn = stream_socket_accept($socket, -1, $peername);
		if($conn ===false)
			die("Socket failed\n");
		
		if (!function_exists('pcntl_fork') || ($pid = pcntl_fork()) == -1)
		{
			$pid = -1;
		}
		if($pid>0) // fork father, wait next socket
		{
			// kill previous zombie
			while(pcntl_wait($status, WNOHANG)>0);
			continue;
		}
		
		answer_gemini($pid, $conn, $conf, $peername);
	}
}

function answer_gemini($pid, $conn, $conf, $peername)
{
	$tlsSuccess = stream_socket_enable_crypto($conn, true, STREAM_CRYPTO_METHOD_TLS_SERVER);
	if ($tlsSuccess !== true)
	{
		fclose($conn);
		if($pid==0) // forked child
			exit;
		else
			return;
	}
	
	$req = stream_get_line($conn, 1024, "\n");
	echo date('Y-m-d H:i:s')."\t".$peername."\t".trim($req)."\n";
	$url_elems = parse_url(trim($req));
	if(empty($url_elems['path']))
		$url_elems['path'] = '/';
	
	$response = false;
	$body = false;
	// check scheme
	if($response === false && $url_elems['scheme'] != 'gemini')
	{
		$response = "59 BAD PROTOCOL\r\n";
	}
	
	$capsule = strtolower($url_elems['host']);

	// check scheme
	if($response === false && !isset($conf->capsules->$capsule))
	{
		$response = "53 UNKOWN CAPSULE\r\n";
	}
	
	if($response === false && strpos(str_replace("\\",'/',rawurldecode($url_elems['path'])),'/..')!==false)
	{
		$response = "59 BAD URL\r\n";
	}
	
	if(!empty($conf->capsules->$capsule->redirect))
	{
		// redirect to another capsule
		$response = "30 ".$conf->capsules->$capsule->redirect.$url_elems['path']."\r\n";
	}
	else
	{
		// search requested file
		$filename = $conf->capsules->$capsule->folder.rawurldecode($url_elems['path']);
		$lang = $conf->capsules->$capsule->lang;
		if(!empty($conf->capsules->$capsule->lang_regex))
		{
			// search lang code in requested path (ex: file.fr.gmi)
			preg_match($conf->capsules->$capsule->lang_regex, rawurldecode($url_elems['path']), $matches);
			if(isset($matches[1]))
				$lang = strtolower($matches[1]);
		}
	}

	if($response === false && file_exists($filename))
	{
		if($response === false && is_file($filename))
		{
			
			$mime = mime_content_type($filename);
			if($mime == "text/plain")
			{
				if(substr($url_elems['path'],-4)=='.gmi')
					$mime = "text/gemini; lang=".$lang;
				elseif(substr($url_elems['path'],-3)=='.md')
					$mime = "text/markdown";
				elseif(substr($url_elems['path'],-4)=='.html')
					$mime = "text/html";
			}
			$response = "20 ".$mime."\r\n";
			$body = file_get_contents($filename);
		}
		
		if($response === false && is_dir($filename))
		{
			// if path is a directory name redirect into it
			if(substr($filename,-1)!='/')
			{
				$response = "30 ".$url_elems['path']."/\r\n";
			}
			else
			{
				$mime = "text/gemini; lang=".$lang;
				if(file_exists($filename.'/index.gmi'))
				{
					// open default file index.gmi
					$response = "20 ".$mime."\r\n";
					$filename = $filename.'/index.gmi';
					$body = file_get_contents($filename);
				}
				elseif(is_array($conf->capsules->$capsule->auto_index_ext))
				{
					// build auto index
					$response = "20 ".$mime."\r\n";
					$body = "# ".$capsule." ".basename($filename)."\r\n";
					$body .= "=> ../ [..]\r\n";
					// three blocks
					$items_dir=array(); // sub directories
					$items_gmi=array(); // gmi file chronogically desc
					$items_oth=array(); // other files
					$d = dir($filename);
					while (false !== ($entry = $d->read()))
					{
						if(substr($entry,0,1)=='.')
						{
							// dir itself
							continue;
						}
						if(is_dir($filename.'/'.$entry) &&
							!in_array('/', $conf->capsules->$capsule->auto_index_ext))
						{
							// folder ext "/" not in auto_index conf
							continue;
						}
						if(is_file($filename.'/'.$entry) &&
							!in_array(substr($entry,strrpos($entry,'.')), $conf->capsules->$capsule->auto_index_ext))
						{
							// ext not in auto_index conf
							continue;
						}						$link_name = $entry;
						if(substr($entry,-4)=='.gmi')
						{
							// build feed for subscriptions for .gmi files,
							// adding date YYYY-MM-DD in link if not file name
							// see specs gemini://gemini.circumlunar.space/docs/companion/subscription.gmi
							$entry_name = str_replace('_',' ',substr($entry,0,-4));
							if(!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])\s$/",substr($entry_name,0,11)))
								$link_name = date("Y-m-d", filemtime($filename.'/'.$entry))." ".$entry_name;
							else
								$link_name = " ".$entry_name;
							$items_gmi[$link_name." ".$entry] = "=> ".rawurlencode($entry)." ".$link_name;
						}
						elseif(is_dir($filename.'/'.$entry))
						{
							// sub directory
							$link_name = "[".$entry."]";
							$items_dir[$entry] = "=> ".rawurlencode($entry)."/ ".$link_name;
						}
						else
						{
							// other file ext
							$items_oth[$entry] = "=> ".rawurlencode($entry)." ".$link_name;
						}
					}
					$d->close();
					ksort($items_dir);
					krsort($items_gmi);
					ksort($items_oth);
					if(count($items_dir)>0)
						$body .= implode("\r\n", $items_dir)."\r\n";
					if(count($items_gmi)>0)
						$body .= implode("\r\n", $items_gmi)."\r\n";
					if(count($items_oth)>0)
						$body .= implode("\r\n", $items_oth)."\r\n";
				}
			}
		}
	}
	
	if($response === false)
	{
		$response = "51 NOT FOUND\r\n";
	}
	
	fputs($conn, $response);
	if($body!==false)
		fputs($conn, $body);
	fflush($conn);
	fclose($conn);
	
	if($pid==0) // forked child
		exit;
	else
		return;
}

