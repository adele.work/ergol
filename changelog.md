# Ergol changelog

[Ergol repository](https://codeberg.org/adele.work/ergol)

## Version 0.4
2021-04-01 09:30:00 UTC
* add multithreading support if PHP PCNTL extension is supported
[PCNTL documentation](https://www.php.net/manual/en/book.pcntl.php)

## Version 0.3
2021-02-20 12:01:00 UTC
* add "lang_regex" parameter in json congif, a regular expression to detect lang code in file name to replace default lang (eg: "fr" in "index.fr.gmi"). It will adapt meta returned to client. 

## Version 0.2
2021-02-19 21:40:23 UTC
* add "redirect" parameter in json config to specify a new capsule host when old one is requested
